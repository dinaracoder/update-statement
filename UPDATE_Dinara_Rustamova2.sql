-- Updating added new film duration and rate
UPDATE film 
SET rental_duration = 21, rental_rate = 9.99
WHERE title = 'Harry Potter and the Philosopher''s Stone';

-- Update customer info
UPDATE customer
SET store_id = 1,
    first_name = 'Dinora',
    last_name = 'Rustamova',
    email = 'Dinora_Rustamova@student.itpu.uz',
    address_id = (SELECT address_id FROM address WHERE address = '123 Main St'),  
    create_date = CURRENT_DATE
WHERE customer_id = 123;
	
	
	
	
	SELECT film.title, film.rental_duration, film.rental_rate, inventory.store_id, actor.first_name ||' '|| actor.last_name as fullname
	FROM film
	JOIN inventory USING(film_id)
	JOIN film_actor USING(film_id)
	JOIN actor USING(actor_id)
	WHERE film.title LIKE '%Harry%';
	

SELECT customer.customer_id, customer.first_name, customer.last_name,
COUNT(rental.customer_id) as rental_count, COUNT(payment.customer_id) as payment_count
FROM customer
JOIN rental USING(customer_id)
JOIN payment USING(customer_id)
GROUP BY customer.customer_id, customer.first_name, customer.last_name

SELECT customer.first_name, customer.last_name, COUNT(payment.payment_id) as payment_count
FROM customer
JOIN payment USING(customer_id)
WHERE customer.first_name LIKE '%AMBER%'
GROUP BY customer.first_name, customer.last_name
HAVING COUNT(payment.payment_id) > 10



SELECT *
FROM customer
WHERE EXISTS (SELECT *
  FROM rental r 
  GROUP BY r.customer_id
  HAVING COUNT(r.rental_id) >= 10 	
  WHERE customer.customer_id = r.customer_id
)
					 
SELECT c.customer_id,  COUNT(p.payment_id) as countp
FROM customer c
JOIN payment p ON c.customer_id = p.customer_id
GROUP BY c.customer_id
ORDER BY COUNT(p.payment_id)


SELECT c.customer_id,  COUNT(r.rental_id) as countp
FROM customer c
JOIN rental r ON c.customer_id = r.customer_id
GROUP BY c.customer_id
ORDER BY COUNT(r.rental_id)
  
SELECT c.customer_id,  COUNT(DISTINCT r.rental_id) as countp, COUNT(DISTINCT p.payment_id) as countp
FROM customer c
JOIN rental r ON c.customer_id = r.customer_id
JOIN payment p ON c.customer_id = p.customer_id
GROUP BY c.customer_id
ORDER BY c.customer_id, COUNT(r.rental_id), COUNT(p.payment_id)
limit(1)

SELECT *
FROM customer 


SELECT *
FROM address
WHERE address = '1190 0 Place'

SELECT customer_id
  FROM rental
  GROUP BY customer_id
  HAVING COUNT(*) >= 10
.

  SELECT customer.customer_id
  FROM customer
  JOIN rental ON customer.customer_id = rental.customer_id
  JOIN payment ON customer.customer_id = payment.customer_id
  GROUP BY customer.customer_id
  HAVING COUNT(rental.rental_id) >= 10 
     AND COUNT(payment.payment_id) >= 10
	 limit(1)